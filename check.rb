require "pwned"

input_array = ARGV

if input_array.length == 0
    puts "Usage: ruby check.rb <password>"
else
    password = Pwned::Password.new(ARGV[0])
    if password.pwned?
        puts "Your password is Pwned for " + password.pwned_count.to_s + " times 😡"
    else
        puts "Your password is secured 😃"
    end
end
